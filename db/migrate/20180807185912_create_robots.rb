class CreateRobots < ActiveRecord::Migration[5.1]
  def change
    create_table :robots do |t|
      t.string :name
      t.integer :speed
      t.integer :strength
      t.integer :agility

      t.timestamps
    end
  end
end
