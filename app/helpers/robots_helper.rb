module RobotsHelper
  # The heavy lifting of creating the team of robot warriors
  def create_team(team_size, subs, salary_cap)
    team = []

    (1..team_size).each do |index|
      robot = Robot.new

      # make sure that the robot name is unique
      name = generate_name
      while team.map{|r| r.name}.include? name
        name = generate_name
      end
      robot.name = name

      available_points = generate_available_points(team_size, salary_cap)
      build_attributes(robot, available_points)

      team.push(robot)
    end

    # Remaining points will be added to bots that have duplicates
    remaining_points = calculate_remaining_points(team, salary_cap)

    # Run this until there are no duplicates
    until fix_duplicates(team)
    end

    remaining_points = calculate_remaining_points(team, salary_cap)

    # Return an empty array if we've run out of points, which means we still have a duplicate
    # Hack right now until I think of a better solution for
    return [] if remaining_points < 0

    # Use all remaining points to create a super star
    create_super_star(team.first, remaining_points) if remaining_points > 0
    remaining_points = calculate_remaining_points(team, salary_cap)

    # One more check to make sure we didn't create a duplicate
    # Although thinking about it that'd be impossible since this
    # robot would have more total points than all the others
    return [] if !fix_duplicates(team)

    team
  end

  def generate_name
    alpha = (0...4).map { (65 + rand(26)).chr }.join
    numeric = rand(1000..9999)

    name = alpha + numeric.to_s
  end

  def build_attributes(robot, available_points)
    # dividing initial amount by two for added randomness
    strength = rand(1..available_points) / 2
    strength += 1 if strength == 0
    robot.strength = strength

    # subtract strength from the available points 
    # subtract 1 to ensure there's at least one point left over for speed
    remaining_points = available_points - strength - 1
    agility = rand(1..remaining_points)
    robot.agility = agility

    robot.speed = available_points - robot.strength - robot.agility
  end

  # This number is the max number of points each robot can have
  # IE: If there's 150 available points, each robot can have 10 points
  def generate_available_points(team_size, salary_cap)
    salary_cap/team_size
  end

  def fix_duplicates(team)
    no_duplicates = true
    # Dual each loop to check for duplicates
    team.each_with_index do |robot, index|
      team.drop(index + 1).each do |teammate|
        # add points to teammate so that it gets checked
        if compare_attributes(robot, teammate)
          no_duplicates = false
          # Adding to speed just for sake of ease
          teammate.speed += 1
        end
      end
    end
    no_duplicates
  end

  def compare_attributes(robot, teammate)
    return robot.speed == teammate.speed && robot.strength == teammate.strength && robot.agility == teammate.agility
  end

  # Salary cap minus all the attribute points added up
  def calculate_remaining_points(team, salary_cap)
    salary_cap - team.map{|r| r.speed + r.agility + r.strength}.sum
  end

  # Simply add the rest of the remaining points to a single robot so that we hit the salary cap
  def create_super_star(robot, remaining_points)
    available_attributes = 3
    points_to_add = remaining_points/available_attributes
    robot.agility += points_to_add
    robot.speed += points_to_add
    robot.strength += points_to_add + remaining_points%available_attributes
  end

end

# All this talk of robots got me thinking
# about the best robot of all time
#       _
#      ( )
#       H
#       H
#      _H_ 
#   .-'-.-'-.
#  /         \
# |           |
# |   .-------'._
# |  / /  '.' '. \
# |  \ \ @   @ / / 
# |   '---------'        
# |    _______|  
# |  .'-+-+-+|  
# |  '.-+-+-+|         
# |    """""" |
# '-.______.-'
