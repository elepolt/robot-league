json.extract! robot, :id, :name, :speed, :strength, :agility, :created_at, :updated_at
json.url robot_url(robot, format: :json)
