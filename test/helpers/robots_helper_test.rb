require 'test_helper'

class RobotsHelperTest < ActionView::TestCase
  test "generate_name returns alpha numeric name" do
    name = generate_name
    assert_not_nil(name)
  end

  test "create_team returns team" do
    team_size = 15
    subs = 5
    salary_cap = 175

    team = create_team(team_size, subs, salary_cap)
    assert_not_nil(team)
  end

  test "each robot on team has unique names" do
    team_size = 15
    subs = 5
    salary_cap = 175

    team = create_team(team_size, subs, salary_cap)
    assert_equal(team_size, team.map{|r| r.name}.count)
  end

  test "generate_available_points does math" do
    available_points = generate_available_points(10, 150)
    assert_equal(15, available_points)
  end

  test "generate_available_points rounds" do
    available_points = generate_available_points(15, 175)
    assert_equal(11, available_points)
  end

  test "build_attributes does not set any attribute to zero" do
    (1..10).each do |i|
      robot = Robot.new
      build_attributes(robot, 11)
      assert_not_equal(0, robot.speed)
      assert_not_equal(0, robot.agility)
      assert_not_equal(0, robot.strength)
    end
  end

  test "calculate_remaining_points does math" do
    salary_cap = 15
    r1 = Robot.new(speed: 3, agility: 2, strength: 1)
    r2 = Robot.new(speed: 3, agility: 2, strength: 2)
    team = [r1, r2]
    remaining_points = calculate_remaining_points(team, salary_cap)
    assert_equal(2, remaining_points)
  end

  test "fix_duplicates returns true when there are no duplicates" do
    r1 = Robot.new(speed: 3, agility: 2, strength: 1)
    r2 = Robot.new(speed: 3, agility: 2, strength: 2)
    team = [r1, r2]

    assert(fix_duplicates(team))
  end

  test "fix_duplicates returns false when there are duplicates" do
    r1 = Robot.new(speed: 3, agility: 2, strength: 2)
    r2 = Robot.new(speed: 3, agility: 2, strength: 2)
    team = [r1, r2]

    assert_not(fix_duplicates(team))
  end

  test "compare_attributes returns true when attributes match" do
    r1 = Robot.new(speed: 3, agility: 2, strength: 2)
    r2 = Robot.new(speed: 3, agility: 2, strength: 2)
    assert(compare_attributes(r1, r2))
  end

  test "compare_attributes returns false when attributes do not match" do
    r1 = Robot.new(speed: 3, agility: 2, strength: 1)
    r2 = Robot.new(speed: 3, agility: 2, strength: 2)
    assert_not(compare_attributes(r1, r2))
  end

  test "create_super_star creates a super star!" do
    robot = Robot.new(speed: 3, agility: 2, strength: 1)
    remaining_points = 2
    create_super_star(robot, remaining_points)
    assert_equal(3, robot.speed)
    assert_equal(2, robot.agility)
    assert_equal(3, robot.strength)
  end

  test "create_super_star evenly distributes remaining points" do
    robot = Robot.new(speed: 3, agility: 2, strength: 1)
    remaining_points = 3
    create_super_star(robot, remaining_points)
    assert_equal(4, robot.speed)
    assert_equal(3, robot.agility)
    assert_equal(2, robot.strength)
  end
end