# README

Ya know how it goes, it's a rails application!

* git clone https://gitlab.com/elepolt/robot-league
* cd robot-league
* bundle install
* rake db:migrate
* rake test
* rails s
* localhost:3000